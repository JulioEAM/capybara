Given("I am on the YouTube home page") do
  # pending # Write code here that turns the phrase above into concrete actions
  visit 'http://www.youtube.com'
end

When(/^I search for "(.*?)"$/) do |search_term|
  fill_in 'search_query', :with => search_term
  click_on 'search-icon-legacy'
end

Then(/^videos of large rodents are returned$/) do
  # pending # Write code here that turns the phrase above into concrete actions
  page.has_content? 'Largest Rodent'
end

Given ("I am on the Pivotal Tracker page") do
  visit 'https://www.pivotaltracker.com/'
  click_on 'Log in'
end

When ("fill a valid user") do
  fill_in 'credentials_username', with: 'julio.acevedo@thincode.com'
  click_on 'NEXT'
end

When ("enter user's password") do
  fill_in 'credentials_password', with: 'E4w0F5e7!'
  click_on 'SIGN IN'
end

Then ("main user's page is shown") do
  page.has_content? 'Workspaces'
end

Given ("I am on the main page from apiblog") do
  visit 'http://localhost:3000/'
end

Then ("see if there are 'Match x' elements") do
  count = 0
  all('.result').each_with_index do |elem, idx|
    count += 1 if elem.text == "Match #{idx + 1}"
  end
  count > 0
end

Capybara.configure do |config|
  config.match = :prefer_exact
  config.exact = false
  config.ignore_hidden_elements = false
  config.visible_text_only = true
end
Capybara.current_session.instance_variable_set(:@touched, false)

When ("fill some random fields") do
  p "Capybara.match: #{Capybara.match}"
  p "Capybara.exact: #{Capybara.exact}"

  page.fill_in 'please complete this', :with => 'foobar'
  page.fill_in 'please complete', :with => 'bazqux'
end

Then ("see if there is a specific 'Match x' element") do
  find('.result').text
  page.has_content? 'foobar'
  page.has_content? 'bazqux'
end

Then ("display for results within a scope, in this case, a scope is a section from page") do
  within('#internet_results') do
    all('.result').each do |elem|
      puts elem.text
    end
  end
end

Then(/^the desired search results are returned$/) do
  page.has_selector? '#local_results'
  expect(page).to have_selector '#local_results'
end

Then(/^the desired search results are returned 2$/) do
  expect(page).to have_content 'Local Match 1'
  expect(first('#m2res1')).to have_content 'Local Match 1'
end

Then(/^find the hidden section$/) do
  find('.section', :visible => true, :wait => 5, :text => 'Capybara R', :match => :first)
end

Then(/^locate local_results section checking values$/) do
  #using CSS selector
  page.has_selector? 'div[id=local_resuls]'
  #using XPath
  page.has_xpath? "//div[@id='local_results']"
end

# Login in to an Edge Pipeline account
Given ("I am on the Edge Pipeline page") do
  visit 'https://www.edgepipeline.com/'
  click_on 'Sign In'
end

When ("I am on Edge Pipeline login page") do
  expect(page).to have_content 'Forgot Password?'
end


When ("enter valid user's email and password") do
  sleep 10
  fill_in 'username', with: 'dicafis'
  fill_in 'password', with: 'E4w0F5e7!'
  # find('#button_action', :visible => true, :wait => 10, :text => 'Sign In', :match => :first)
  click_on 'button_action'
end

Then ("main user's dashboard is shown") do
  expect(page).to have_content 'In-Lane Auctions'
end
