Feature: Login into an Edge Pipeline account

  Scenario: Succesful login into an Edge Pipeline account
    Given I am on the Edge Pipeline page
    When I am on Edge Pipeline login page
    When enter valid user's email and password
    Then main user's dashboard is shown
