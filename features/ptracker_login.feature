Feature: Login into a PivotalTracker account

  Scenario: Succesful login into a PivotalTracker account
    Given I am on the Pivotal Tracker page
    When fill a valid user
    When enter user's password
    Then main user's page is shown
