Feature: Using matching strategies

  Scenario: Given a list, find an element on it
    Given I am on the main page from apiblog
    Then see if there are 'Match x' elements

  Scenario: Given a list, find an element on it
    Given I am on the main page from apiblog
    When fill some random fields
    Then see if there is a specific 'Match x' element
